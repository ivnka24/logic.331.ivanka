package BangunDatar;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);

    public static void luas() {
        //luas
        System.out.println("Menghitung Luas Trapesium");
        System.out.println("Masukan Sisi atas");
        int sisiatas = input.nextInt();

        System.out.println("Masukan Sisi bawah");
        int sisibawah = input.nextInt();

        System.out.println("Masukan tinggi trapesium");
        int tinggitrapesium = input.nextInt();

        double luastrapesium = (int) (0.5 * (sisiatas + sisibawah) * tinggitrapesium);
        System.out.println("Luas Trapesium adalah " + luastrapesium);
    }

    public static void keliling() {
//        keliling
        System.out.println("Menghitung Keliling Trapesium");
        System.out.println("Masukan sisi miring 1");
        int sisimiring1 = input.nextInt();

        System.out.println("Masukan sisi miring 2");
        int sisimiring2 = input.nextInt();

        System.out.println("Masukan sisi miring 3");
        int sisimiring3 = input.nextInt();

        System.out.println("Masukan sisi miring 4");
        int sisimiring4 = input.nextInt();

        double keliling_trapesium = (int) (sisimiring1 + sisimiring2 + sisimiring3 + sisimiring4);
        System.out.println("Keliling Trapesium adalah " + keliling_trapesium);
    }

}

