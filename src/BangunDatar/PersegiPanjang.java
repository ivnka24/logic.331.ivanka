package BangunDatar;

import java.util.Scanner;

public class PersegiPanjang {
    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;

    public static void Luas() {
        System.out.println("Masukan Panjang");
        panjang = input.nextInt();

        System.out.println("Masukan Lebar");
        lebar = input.nextInt();

        double LuasPersegi = panjang * lebar ;
        System.out.println("Luas Persegi adalah = " + +LuasPersegi);
    }

    public static void Keliling() {
        System.out.println("Masukan panjang");
        panjang = input.nextInt();

        System.out.println("Masukan Lebar");
        lebar = input.nextInt();

        double kelilingPersegi = (int) (2 * panjang) + (2 * lebar);
        System.out.println("Keliling Persegi adalah = " + kelilingPersegi);
    }
}
