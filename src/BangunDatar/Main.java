package BangunDatar;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.

import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//       int age = 23;
//       String name = "Ivanka Mauludi Juniar";
//       String hobby = "Fishing";
//
//        System.out.println("Hello World");
//        System.out.println("My Name Is " + name);
//        System.out.println("I am " + age + " years old");
//        System.out.println("My Hobby is " + hobby);
//
//        String text = "wikwik";
//        Scanner inputtext = new Scanner(System.in);
//        System.out.println("Tebak text Bro :");
//        String tebak = inputtext.nextLine();
//
//        if(text.equals(tebak)){
//            System.out.println("YEAY DAPET 100 M");
//        }else{
//            System.out.println("GA DAPET BONUS BRO");
//        }

        //Luas Persegi Panjang
        Scanner input = new Scanner(System.in);

        boolean flag = true;
        String answer = "y";

        while(flag){
            System.out.println("Pilih Bangun Datar");
            System.out.println("1. Persegi Panjang");
            System.out.println("2. Segitiga");
            System.out.println("3. Trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");
            System.out.println("----------------------------");
            int pilih1 = input.nextInt();
            int pilih2;
            String prompt = "Pilihan        1. Luas        2. Keliling";
            switch (pilih1) {
                case 1:
                    System.out.println(prompt);
                    pilih2 = input.nextInt();
                    if (pilih2 == 1) {
                        PersegiPanjang.Luas();

                    } else if (pilih2 == 2) {
                        PersegiPanjang.Keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 2:
                    System.out.println(prompt);
                    pilih2 = input.nextInt();
                    if (pilih2 == 1) {
                        Segitiga.Luas();

                    } else if (pilih2 == 2) {
                        Segitiga.Keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 3:
                    System.out.println(prompt);
                    pilih2 = input.nextInt();
                    if (pilih2 == 1) {
                        Trapesium.luas();

                    } else if (pilih2 == 2) {
                        Trapesium.keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 4:
                    System.out.println(prompt);
                    pilih2 = input.nextInt();
                    if (pilih2 == 1) {
                        Lingkaran.Luas();

                    } else if (pilih2 == 2) {
                        Lingkaran.Keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 5:
                    System.out.println(prompt);
                    pilih2 = input.nextInt();
                    if (pilih2 == 1) {
                        Persegi.luas();

                    } else if (pilih2 == 2) {
                        Persegi.keliling();
                    } else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                default:
                    System.out.println("Pilhan tidak tersedia");
            }
            System.out.println("Try again? y/n");
            input.nextLine();//skipbro
            answer = input.nextLine();

            if(!answer.toLowerCase().equals("y")){
                flag = false;
            }
        }
    }
}