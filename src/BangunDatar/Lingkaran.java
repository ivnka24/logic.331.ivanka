package BangunDatar;

import java.util.Scanner;

public class Lingkaran {
    private static Scanner input = new Scanner(System.in);
    private static double phi = 3.14;
    private static double jari2;

    public static void Luas() {
        System.out.println("Masukan Jari-Jari : ");
        jari2 = input.nextDouble();

        double luaslingkaran = (int) (phi * jari2 * jari2);
        System.out.println("Luas lingkaran adalah " + luaslingkaran);
    }

    public static void Keliling() {
        System.out.println("Masukan Jari-Jari : ");
        jari2 = input.nextDouble();
        double kelilinglingkaran = (int) (2 * phi * jari2);
        System.out.println("Keliling Lingkaran adalah " + kelilinglingkaran);
    }
}
