package BangunDatar;

import java.util.Scanner;

public class Persegi {
    private static Scanner input = new Scanner(System.in);
    private static int sisi;

    public static void luas() {
        System.out.println("Masukan Sisi : ");
        sisi = input.nextInt();
        int LuasPersegi = (int) (sisi * sisi);
        System.out.println("Luas Persegi adalah = " + LuasPersegi);
    }

    public static void keliling() {
        System.out.println("Masukan Sisi : ");
        sisi = input.nextInt();
        int KelilingPersegi = (int) (4 * sisi);
        System.out.println("Keliling Persegi adalah = " + KelilingPersegi);
    }
}
