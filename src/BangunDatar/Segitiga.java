package BangunDatar;

import java.util.Scanner;

public class Segitiga {
    private static Scanner input = new Scanner(System.in);
    private static int alas;
    private static int tinggi;

    public static void Luas() {
        System.out.println("Menghitung Luas Segitiga");

        System.out.println("Masukan alas segitiga");
        alas = input.nextInt();

        System.out.println("Masukan tinggi segitiga");
        tinggi = input.nextInt();

        double LuasSegitiga = (0.5 * alas * tinggi);
        System.out.println("Luas Seigitiga adalah = " + LuasSegitiga);
    }

    public static void Keliling() {
        System.out.println("Menghitung Keliling Segitiga");

        System.out.println("Masukan Sisi 1");
        int sisi1 = input.nextInt();

        System.out.println("Masukan Sisi 2");
        int sisi2 = input.nextInt();

        System.out.println("Masukan Sisi 3");
        int sisi3 = input.nextInt();

        int kelilingsegitiga = (sisi1 + sisi2 + sisi3);
        System.out.println("Keliling segitiga adalah = " + kelilingsegitiga);
    }
}
