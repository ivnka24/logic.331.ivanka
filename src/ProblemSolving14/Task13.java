package ProblemSolving14;

import java.util.Scanner;

public class Task13 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        System.out.println("Masukan aplhabet :");
        String inputAlphabet = input.nextLine();
        System.out.println("Masukan angka :");
        String numberSplit = input.nextLine();
        char[] inputChar = inputAlphabet.toCharArray();
        String[] inputNumber = numberSplit.split(", ");
        String[] output = new String[inputNumber.length];
        for (int i = 0; i < inputNumber.length; i++) {
            int numbers = Integer.parseInt(inputNumber[i]);
            char huruf = inputChar[i];
            if(alphabet[numbers-1] == huruf){
                output[i] = "true";
            }else{
                output[i] = "false";
            }
        }
        for (String temp:output) {
            System.out.print(temp+", ");
        }
    }
}
