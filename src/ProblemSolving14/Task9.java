package ProblemSolving14;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Task9 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String masuk, keluar;
        long durasiParkir = 0, biayaParkir, jamParkir;
        Date dateMasuk = null, dateKeluar = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");

        System.out.print("Tanggal dan jam masuk: ");
        masuk = input.nextLine();

        System.out.print("Tanggal dan jam keluar: ");
        keluar = input.nextLine();

        try {
            dateMasuk = dateFormat.parse(masuk);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        try {
            dateKeluar = dateFormat.parse(keluar);
        }
        catch (Exception e){
            e.printStackTrace();
        }

        durasiParkir = dateKeluar.getTime() - dateMasuk.getTime();

        jamParkir = durasiParkir / (1000 * 60 * 60);

        if (jamParkir <= 8){
            biayaParkir = jamParkir * 1000;
        }
        else if (jamParkir > 8 && jamParkir < 24) {
            biayaParkir = 8000;
        }
        else {
            biayaParkir = (jamParkir / 24) * 15000;
        }

        System.out.println("Tarif Parkir: " + biayaParkir);
    }
}
