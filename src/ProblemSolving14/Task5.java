package ProblemSolving14;

import java.util.Scanner;

public class Task5 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan jam : ");
        String textDate = input.nextLine();
        String numberConvert = textDate.substring(0, 2);

        int jam = Integer.parseInt(numberConvert);
        String menit = textDate.substring(3, 5);
//        System.out.println(menit);
        //07:35 AM
        //01234567
        String output = null;
        if (textDate.toLowerCase().contains("am") || (textDate.toLowerCase().contains("pm"))) {
            if (textDate.contains("am")) {
                output = textDate.substring(0, 5);
            } else if (textDate.contains("pm")) {
                if (jam != 12) {
                    output = (jam + 12) + ":" + menit;
                } else {
                    output = "00" + ":" + menit;
                }
            }
        } else {
            if (jam > 12) {
                output = (jam - 12) + ":" + menit + " PM";
            } else {
                output = (jam) + ":" + menit + " AM";
            }
        }
        System.out.println(output);
    }
}

