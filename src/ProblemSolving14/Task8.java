package ProblemSolving14;

import java.util.*;

import static java.util.Arrays.sort;

public class Task8 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukan jumlah uang andi : ");
        int uangAndi = input.nextInt();
        System.out.println();
        input.nextLine();
        System.out.print("Harga Kacamata : ");
        String textHargaKacamata = input.nextLine();
        System.out.print("Harga Baju : ");
        String textHargaBaju = input.nextLine();
        String[] splitKacamata = textHargaKacamata.split(", ");
        String[] splitBaju = textHargaBaju.split(", ");

        int[] tambah = new int[splitKacamata.length * splitBaju.length];
        int count = 0;
        for (int i = 0; i < splitKacamata.length; i++) {
            for (int j = 0; j < splitBaju.length; j++) {
                int nilaiKacamata = Integer.parseInt(splitKacamata[i]);
                int nilaiBaju = Integer.parseInt(splitBaju[j]);
                tambah[count] = nilaiKacamata + nilaiBaju;
                count++;
            }
        }
//        ArrayList<Integer> rekomendasi = new ArrayList<Integer>();
        int rekomendasi = 0;
        for (int i : tambah) {
            if (uangAndi >= i && rekomendasi < i) {
                rekomendasi = i;
            }
        }
//        Collections.sort(rekomendasi);
//        int lastValue = (rekomendasi.size()-1);
        System.out.println("Output " + rekomendasi);
    }
}
