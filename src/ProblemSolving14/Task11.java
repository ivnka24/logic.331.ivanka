package ProblemSolving14;

import java.util.Scanner;

public class Task11 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan jumlah pembelian pulsa (dalam Rupiah): ");
        int inputPulsa = input.nextInt();

        int points = 0;
        int maxPoints = 0;
        int remainingPoint = 0;
        int remainingSecond = 0;
        if (inputPulsa <= 10000) {
            points = 0;
            System.out.println("Output: "+ points);
        } else if (inputPulsa <= 30000) {
            remainingPoint = (inputPulsa - 10000) / 1000;
            maxPoints = points + remainingPoint;
            System.out.println("Output: " + points + "+ " + remainingPoint + " = " + maxPoints);
        } else {
            remainingPoint = (20000 / 1000);
            remainingSecond = ((inputPulsa - 30000) / 1000) * 2;
            maxPoints = points+remainingPoint+remainingSecond;
            System.out.println("Output: " + points + " + " + remainingPoint +" + " + remainingSecond + " = " + maxPoints);
        }
    }
}
