package ProblemSolving14;

import java.util.Scanner;
//2. Bambang adalah karyawan grosir X yang bertugas mengantarkan barang ke toko-toko pelanggannya yang terletak di satu ruas jalan panjang yang sama, dengan jarak masing-masing sebagai berikut (dihitung dari grosir X):
//        Toko 1: 0.5 km
//        Toko 2: 2 km
//        Toko 3: 3.5 km
//        Toko 4: 5 km
//
//        Jika Bambang menghabiskan waktu rata-rata 10 menit di setiap toko, dan laju rata-rata motor Bambang adalah 30 km/jam, berapa lama waktu yang dibutuhkan Bambang untuk mengantarkan pesanan hingga kembali ke grosir?
//
//        Input: 1-2-3-4
//        Output: 60 menit
//        clue: jarak total adalah 10 km
//
//        Input: 1-3-2-4-1
//        Output: 76 menit
//        clue: jarak total adalah 13 km
public class Task2 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        double [] jarakToko = {0, 0.5, 2, 3.5, 5};
        double jarakAwal = 0;
        double dumpJarak = 0;
        double totalJarak = 0;
        int toko = 0;
        System.out.println("Masukan deret angka :");
        String text = input.nextLine();
        int [] inputArray = Utility.ConvertStringToArrayInt(text);
        for (int i = 0; i < inputArray.length; i++) {
            toko = inputArray[i]; //menyimpan posisi toko
            dumpJarak = jarakToko[toko] - jarakAwal; //untuk menyimpan toko sebelumnya
            totalJarak +=Math.abs(dumpJarak); //
            jarakAwal = jarakToko[toko];
        }
        totalJarak = totalJarak + jarakAwal;
        double Kecepatan = 0.5; //0.5km/menit
        double lamaJalan = totalJarak / Kecepatan; //
        double outputAkhir = lamaJalan + (10 * inputArray.length);
        System.out.println(outputAkhir);
    }
}
