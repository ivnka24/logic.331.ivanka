package ProblemSolving14;

public class Utility {
    public static void PrintArray2D(int[][] hasil) {
        for (int i = 0; i < hasil.length; i++) {
            for (int j = 0; j < hasil[0].length; j++) {
                System.out.print(hasil[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int[] ConvertStringToArrayInt(String text) {
        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }
        return intArray;
    }
}
