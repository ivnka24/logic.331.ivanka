package ProblemSolving14;

import java.util.Scanner;

public class Task10 {
    private static Scanner input = new Scanner(System.in);
    private static String[] split = new String[2];

    public static void Resolve() {
        System.out.println("Masukan Jumlah dan Jenis");
        String text = input.nextLine();
        split = text.split(" ");
        Task10.menu();
        int choose = input.nextInt();
        switch (choose) {
            case 1:
                calculateTeko(Integer.parseInt(split[0]), split[1]);
                break;
            case 2:
                calculateBottle(Integer.parseInt(split[0]), split[1]);
                break;
            case 3:
                calculateGelas(Integer.parseInt(split[0]), split[1]);
                break;
            case 4:
                calculateCangkir(Integer.parseInt(split[0]), split[1]);
                break;
            default:
        }
    }
    // teko ==> cangkir ==> botol ==> gelas
    //   1  ==>    25   ==>   5   ==>  10
    //      x25         /5         *2

    // teko ==> botol ==> gelas ==> cangkir
    //   1  ==>   5   ==>  10   ==>   25
    //       *5         *2         *2,5
    public static void calculateBottle(int quantity, String glass) {
        double totalConvert;
        String textConvertsion = "Botol";
        if (glass.equalsIgnoreCase("teko")) {
            totalConvert = (double) quantity * 5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("gelas")) {
            totalConvert = (double) quantity / 2;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("cangkir")) {
            totalConvert = (double) (quantity / 2.5) / 2;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else {
            System.out.println(textConvertsion + ": " + quantity);
        }
    }

    public static void calculateTeko(int quantity, String glass) {
        double totalConvert;
        String textConvertsion = "Teko";
        if (glass.equalsIgnoreCase("botol")) {
            totalConvert = quantity / 5.0;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("gelas")) {
            totalConvert = (double) (quantity / 5) / 2;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("cangkir")) {
            totalConvert = (double) ((quantity / 5) / 2) / 2.5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else {
            System.out.println(textConvertsion + ": " + quantity);
        }
    }

    public static void calculateGelas(int quantity, String glass) {
        double totalConvert;
        String textConvertsion = "Gelas";
        if (glass.equalsIgnoreCase("botol")) {
            totalConvert = (double) quantity * 2;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("teko")) {
            totalConvert = (double) (quantity * 5) * 2;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("cangkir")) {
            totalConvert = (double) quantity / 2.5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else {
            System.out.println(textConvertsion + ": " + quantity);
        }
    }

    public static void calculateCangkir(int quantity, String glass) {
        double totalConvert;
        String textConvertsion = "Cangkir";
        if (glass.equalsIgnoreCase("botol")) {
            totalConvert = (double) (quantity * 2) * 2.5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("teko")) {
            totalConvert = (double) ((quantity * 5) * 2) * 2.5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else if (glass.equalsIgnoreCase("gelas")) {
            totalConvert = (double) quantity * 2.5;
            System.out.println(textConvertsion + ": " + totalConvert);
        } else {
            System.out.println(textConvertsion + ": " + quantity);
        }
    }

    public static void menu() {
        System.out.println("Ingin dikonversi ke ?");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        System.out.println("Jawab = ");
    }
}
