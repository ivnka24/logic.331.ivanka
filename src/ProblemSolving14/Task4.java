package ProblemSolving14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
//4. Andi memiliki sejumlah uang dan wishlist di e-commerce. Jika Andi ingin membeli sebanyak mungkin barang dari wishlist-nya, barang apa sajakah yang bisa dibeli Andi dengan uang tersebut? Jika ada barang dengan harga yang sama, yang diprioritaskan adalah yang pertama di-input.
//
//        Input: Uang Andi, Jumlah barang, nama masing-masing barang dan harganya
//        Output: nama-nama barang yang bisa dibeli
//
//        Contoh:
//        Uang Andi: 100.000
//        Jumlah barang: 6
//        Nama barang:
//        - Kaos batman, 50.000
//        - Sepatu, 90.000
//        - Dompet, 30.000
//        - Pomade, 50.000
//        - Casing handphone, 20.000
//        - Cologne, 50.000
//
//        Output: Kaos batman, Casing handphone, Dompet

public class Task4 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input Uang Andi :");
        int uang = input.nextInt();
        System.out.println("Input jumlah barang : ");
        int jumlahBarang = input.nextInt();
        System.out.println("Contoh input 'barang, harga' ");
        System.out.println("input nama barang:");
        input.nextLine();
//        String [] results = new String[];
        String[][] wishList = new String[2][jumlahBarang];
        for (int i = 0; i < jumlahBarang; i++) {
            String text = input.nextLine();
            String[] split = text.split(", ");
            for (int j = 0; j < split.length; j++) {
                wishList[j][i] = split[j];
            }
        }
        System.out.println("Output : ");
        String results = "";
        for (int i = 0; i < jumlahBarang; i++) {
            String barang = wishList[0][i];
            int harga = Integer.parseInt(wishList[1][i]);
            if(uang >= harga){
                uang-=harga;
                results +=barang+", ";
            }
        }
        System.out.println(results);
    }
}
