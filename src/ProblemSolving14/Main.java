package ProblemSolving14;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-14)");
        int pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 14) {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        switch (pilihan) {
            case 1:
                Task1.Resolve();
//                exampleReturn.example();
                break;
            case 2:
                Task2.Resolve(); // sudah selesai tapi belom mengerti logic dan prosesnya
                break;
            case 3:
                Task3.Resolve();
                break;
            case 4:
                Task4.Resolve(); //paham sedikit tapi belom PD Presentasi
                break;
            case 5:
                Task5.Resolve(); //lumayan paham sedikit , cuma masalah substring atau index string sedikit perlu di pahami
                break;
            case 6:
                System.out.println("belum selesai");
                break;
            case 7:
                Task7.Resolve(); //proses permasalahan logic udah paham sedikit
                break;
            case 8:
                Task8.Resolve(); //
                break;
            case 9:
                System.out.println("belum selesai");
                break;
            case 10:
                Task10.Resolve();//sudah selesai ga ngerti proses cara kerja logic nya hanya memahami sedikit cara kerjanya
                break;
            case 11:
                Task11.Resolve();
                break;
            case 12:
                System.out.println("belum selesai");
                break;
            case 13:
                Task13.Resolve();
                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
    }
}