        package ProblemSolving14;
//7. Jim diminta untuk menyusuri lintasan linier dengan lubang-lubang yang harus dilompati.
//        Jika Jim memilih untuk berjalan, dia akan berpindah tempat sebesar 1 dan mendapatkan energi sebesar 1, tetapi jika dia melompat dia akan berpindah tempat sebesar 2 dan kehilangan energi sebesar 2.
//        Di awal, energi Jim adalah 0. Dengan kombinasi jalan-lompat yang kamu pilih, apakah Jim mampu melewati lintasan itu? Jika mampu, berapakah energi akhirnya? (jika tidak mampu, tulis saja \'Jim died\').
//        Lubangnya selalu berada di antara jalan, tidak ada lubang berturut-turut.
//        Jim boleh melompat kapanpun, tidak harus pada lubang saja.
//        Jika Jim salah memperhitungkan lompat/jalan, dia bisa masuk ke lubang dan mati.
//
//        Input:
//        1. Pola lintasan (- melambangkan jalan, o melambangkan lubang)
//        2. Pilihan cara jalan (w jalan, j lompat)
//
//        Output: energi akhir
//
//        Contoh:
//        1. ---o-o-
//        2. wwwjj
//        3. Jim died (Jim tidak punya tenaga untuk melompati lubang kedua)
//
//        1. -----o----o----o-
//        2. wwwwwjwwwjwwwj
//           11111211121112
//        3. 5
//
//        1. -----o----o----o-
//        2. wwwwwjjwjwwwj
//           1111122121112
//        3. 1
//
//        1. -----o----o----o-
//        2. wwjwwj
//             112112
//        3. Jim died (setelah lompatan pertama, Jim tidak punya tenaga untuk melewati lubang pertama)
//
//        1. -----o----o----o-
//        2. wwwwjwwjwj
//        3. Jim died (lompatan pertamanya mengakibatkan Jim jatuh tepat di lubang pertama)

//breakdown masalah
//1.jim berjalan mengikuti route (-) jallan (0) lubang
//2.jim memiliki 2 pilihan antara (w  = jalan, j = lompat)
//3.ketika ada lubang loncat dua kali menghabiskan 2 energi
//4.ketika ada energy dan dia lompat menambahkan energi 2
//4.ketika dia jalan dia akan bertambah 1 energinya
//5.total energi ketika 0 jika akan mati

import java.util.Scanner;

public class Task7 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan pola lintasan");
        String track = input.nextLine();
        System.out.println("Masukkan pilihan cara jalan (w = jalan , j = lompat)");
        String moves = input.nextLine();
        char[] trackChar = track.toCharArray();
        char[] movesChar = moves.toCharArray();
        int energy = 0;
        int skip = 0;
        String output = "";
        for (int i = 0; i < movesChar.length; i++) {
            char getTrack = trackChar[i + skip];
            char getMoves = movesChar[i];
            if (getMoves == 'w' && getTrack == '-') {
                energy++;
                output = String.valueOf(energy);
            } else if (getMoves == 'w' && getTrack == 'o') {
                System.out.println("Jim is dead");
                break;
            } else if ((getMoves == 'j' && getMoves == 'w') || (getTrack == 'o') && (energy >= 2)) {
                skip = +1;
                energy -= 2;
                output = String.valueOf(energy);
            } else if (getMoves == 'j' && energy < 2) {
                System.out.println("Jim is dead");
                break;
            }
        }
        System.out.println("Output" + output);
    }
}