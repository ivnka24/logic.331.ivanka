package Array2D;

import java.util.Scanner;

public class Task10 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input n");
        int n = input.nextInt();
        int first = 0;
        int second = 0;
        int thrid = 0;
        int [][] results = new int [3][n];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0){
                    results[i][j] = first;
                    first++;
                }else if (i == 1){
                    results[i][j] = second;
                    second+=3;
                }else if (i == 2){
                    results[i][j] = thrid;
                    thrid+=4;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
