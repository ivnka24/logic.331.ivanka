package Array2D;

import java.util.Scanner;

public class Task2 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();
        int[][] results = new int[2][n];
        int baris = 0;
        int kolom = 1;
        int trigger = 4;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    results[i][j] = baris;
                    baris += 1;
                } else if (i == 1) {
                    if (trigger % 3 == 0) {
                        kolom *= 3;
                        results[i][j] = kolom * -1;
                        trigger++;
                    } else if (j == 0) {
                        results[i][j] = kolom;
                        trigger++;
                    } else if (j>=1){
                        kolom *= 3;
                        results[i][j] = kolom;
                        trigger++;
                    }
                }
            }
        }
        Utility.PrintArray2D(results);

    }
}




