package Array2D;

import java.util.Scanner;

public class Task7 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input n");
        int n = input.nextInt();
        int first = 0;
        int [][] results = new int [3][n];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    if((j)%3 == 0){
                        results[i][j] = first++*-1;
                    }else {
                        results[i][j] = first++;
                    }
                }else if(i == 1){
                    if((j+1)%3 == 0){
                        results[i][j] = first++*-1;
                    }else{
                        results[i][j] = first++;
                    }
                }else if (i == 2){
                    if(first%3 == 0){
                        results[i][j] = first++*-1;
                    }else{
                        results[i][j] = first++;
                    }
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
