package Array2D;

import java.util.Scanner;

public class Task6 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("Input n");
        int n = input.nextInt();
        int baris = 0;
        int barisX = 1;
        int t = 0;
        int x = 1;
        int [][] results = new int [3][n];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j] = baris++;
                } else if (i == 1) {
                    results[i][j] = barisX;
                    barisX *= 7;
                } else if(i == 2) {
                    results[i][j] = t + x;
                    t++;
                    x*=7;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
