package Array2D;

import java.util.Scanner;

public class Task3 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();
        int[][] results = new int[2][n];
        int baris = 0;
        int kolom = 3;
        int mid ;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    results[i][j] = baris;
                    baris += 1;
                } else if (i == 1) {
                    results[i][j] = kolom;
                    mid = n/2;
                    if (j > mid) {
                        kolom/=2;
                        results[i][j]=kolom;
//                    } else if(j == 0){
//                        results[i][j] = kolom;
                    } else if (j >= 1 && j <= mid){
                        kolom*=2;
                        results[i][j] = kolom;
                    }
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
