package Array2D;

import java.util.Scanner;

public class Task4 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();
        int[][] results = new int[2][n];
        int baris = 0;
        int kolom = 1;
        int start = 5;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    results[i][j] = baris;
                    baris += 1;
                } else if (i == 1) {
                    if ((j + 1) % 2 == 1) {
                        results[i][j] = kolom;
                        kolom+=1;
                    } else {
                        results[i][j] = start;
                        start += 5;
                    }
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
