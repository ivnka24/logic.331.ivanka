package Array2D;

import java.util.Scanner;

public class Task5 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input n");
        int n = input.nextInt();
        int[][] results = new int[3][n];
        int helper = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
               if(i == 0){
                   results[i][j] = helper;
                   helper++;
               }else if(i == 1){
                   results[i][j] = helper;
                   helper++;
               }else if(i == 2){
                   results[i][j] = helper;
                   helper++;
               }
            }
        }
        Utility.PrintArray2D(results);
    }
}
