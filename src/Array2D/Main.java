package Array2D;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-12)");
        int pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12) {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        switch (pilihan) {
            case 1:
//                Example.Resolve();
                Task1.Resolve();
//                Task1.Resolve(n);
                break;
            case 2:
                Task2.Resolve();
                break;
            case 3:
                Task3.Resolve();
                break;
            case 4:
                Task4.Resolve();
                break;
            case 5:
                Task5.Resolve();
                break;
            case 6:
                Task6.Resolve();
                break;
            case 7:
                Task7.Resolve();
                break;
            case 8:
                Task8.Resolve();
                break;
            case 9:
                Task9.Resolve();
                break;
            case 10:
                Task10.Resolve();
                break;
            case 11:
                Task11.RightTriangle();
                break;
            case 12:
                Task12.Resolve();
                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
    }
}