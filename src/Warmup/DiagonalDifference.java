package Warmup;

import java.util.Scanner;

public class DiagonalDifference {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan jumlah baris dan kolom :");
        int barisKolom = input.nextInt();
        int[][] diagonalArray = new int[barisKolom][barisKolom];
        int sum;
        System.out.println("Masukan angka : ");
        input.nextLine();
        for (int i = 0; i < barisKolom; i++) {
            String text = input.nextLine();
            String[] numbers = text.split(" ");
            for (int j = 0; j < barisKolom; j++) {
                diagonalArray[i][j] = Integer.parseInt(numbers[j]);
            }
        }
        int sumDiagonal1 = 0;
        int sumDiagonal2 = 0;
        for (int i = 0; i < diagonalArray.length; i++) {
            int hasil1 = sumDiagonal1 += diagonalArray[i][i];
            int hasil2 = sumDiagonal2 += diagonalArray[i][diagonalArray.length-1-i];
        }
        int results = Math.abs(sumDiagonal1 - sumDiagonal2);
        System.out.println("Results "+results);
    }
}
