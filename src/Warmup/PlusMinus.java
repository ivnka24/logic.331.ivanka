package Warmup;

import java.util.Scanner;

public class PlusMinus {
    public static void Resolve (){
        Scanner input = new Scanner(System.in);
        String number = input.nextLine();
        int [] arrayNumber = Utility.ConvertStringToArrayInt(number);
        int zero = 0;
        int minus = 0;
        int positive = 0;
        for (int i = 0; i < arrayNumber.length; i++) {
            if(arrayNumber[i] == 0){
                zero ++;
            }else if (arrayNumber[i] > 0){
                minus ++;
            } else if (arrayNumber[i] < 0) {
                positive ++;
            }
        }
        double resultPositive =(double) positive/ arrayNumber.length;
        double resultMinus = (double) minus/ arrayNumber.length;
        double resultZero = (double) zero/ arrayNumber.length;

        System.out.println("Hasil pada positive adalah "+resultPositive+ ", "+ "negatif: " + resultMinus + ", " + "zero: " +resultZero);
    }
}
