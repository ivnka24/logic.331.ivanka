package Warmup;

import java.util.Scanner;

public class AVeryBigSum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan jumlah deret :");
        int length = input.nextInt();
        input.nextLine();
        System.out.println("Masukan deret angka :");
        String textInput = input.nextLine();
        String [] textSplit = textInput.split(" ");
        long [] longNumber = new long[textSplit.length];
        long sum = 0;
        for (int i = 0; i < textSplit.length; i++) {
            longNumber[i] = Long.parseLong(textSplit[i]);
            sum += longNumber[i];
        }
        System.out.println(sum);
    }
}
