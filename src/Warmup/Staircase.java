package Warmup;

import java.util.Scanner;

public class Staircase {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan jumlah baris");
        int baris = input.nextInt();
        for (int i = 1; i < baris; i++) {
            for (int j = 1; j <= baris - i; j++) {
                System.out.print("+");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
