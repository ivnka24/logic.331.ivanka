package Warmup;

public class Utility {
    public static int[] ConvertStringToArrayInt(String text) {
        String[] textArray = text.split(" ");
        int[] intArray = new int[textArray.length];

        for (int i = 0; i < textArray.length; i++) {
            intArray[i] = Integer.parseInt(textArray[i]);
        }
        return intArray;
    }

    public static int[][] convertStringTo2DArrayInt(String text) {
        String[] rowStrings = text.split(";"); // Split rows using a delimiter (e.g., semicolon)
        int numRows = rowStrings.length;

        String[] colStrings = rowStrings[0].split(" ");
        int numCols = colStrings.length;

        int[][] intArray = new int[numRows][numCols];

        for (int i = 0; i < numRows; i++) {
            String[] elements = rowStrings[i].split(" ");
            for (int j = 0; j < numCols; j++) {
                intArray[i][j] = Integer.parseInt(elements[j]);
            }
        }
        return intArray;
    }
    public static void PrintArray(int [] number){
        for (int i = 0; i < number.length; i++) {
            System.out.print(number[i]+" ");
        }
    }


}
