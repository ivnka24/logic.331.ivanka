package Warmup;

import java.util.Scanner;

public class SimpleArraySum {

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Input deret angka");
        String text = input.nextLine();
        int [] arraySum = Utility.ConvertStringToArrayInt(text);
        int length = arraySum.length;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += arraySum[i];
        }
        System.out.println("SUM"+" : "+sum);
    }

}
