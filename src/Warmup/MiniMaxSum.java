package Warmup;

import Warmup.Utility;

import java.util.Scanner;

public class MiniMaxSum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        int[] arrayNumber = Utility.ConvertStringToArrayInt(text);
        int length = arrayNumber.length;
        int sumIndex = 0;
        for (int number : arrayNumber) {
            sumIndex += number;
        }
        int[] sumException = new int[length];
        for (int i = 0; i < length; i++) {
            sumException[i] = sumIndex - arrayNumber[i];
        }
        int max = sumException[0];
        int min = sumException[0];
        for (int i = 0; i < length; i++) {
            if (sumException[i] < min) {
                min = sumException[i];
            }
            if (sumException[i] > max) {
                max = sumException[i];
            }
        }
        System.out.println(min + " " + max);
    }
}
