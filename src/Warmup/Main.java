package Warmup;

import InputPlural.Task1;
import InputPlural.Task2;
import InputPlural.Task3;
import InputPlural.Task4;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-12)");
        int pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12) {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        switch (pilihan) {
            case 1:
                SolveMeFirst.Resolve();
                break;
            case 2:
                timeConversion.Resolve();
                break;
            case 3:
                SimpleArraySum.Resolve();
                break;
            case 4:
                DiagonalDifference.Resolve();
                break;
            case 5:
                PlusMinus.Resolve();
                break;
            case 6:
                Staircase.Resolve();
                break;
            case 7:
                MiniMaxSum.Resolve();
                break;
            case 8:
                BirthdayCakeCandles.Resolve();
                break;
            case 9:
                AVeryBigSum.Resolve();
                break;
            case 10:
                CompareTheTriples.Resolve();
                break;
            case 11:

                break;
            case 12:

                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
    }
}