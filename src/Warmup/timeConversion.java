package Warmup;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class timeConversion {
    public static String timeConversion (String s){
        try {
            SimpleDateFormat duaBelasJam = new SimpleDateFormat("hh:mm:ssa");
            SimpleDateFormat duaEmpatJam = new SimpleDateFormat("hh:mm:ss");

            Date stringToHours = duaBelasJam.parse(s);
            String hoursto24hours = duaEmpatJam.format(stringToHours);
            return hoursto24hours;
        }catch (Exception e){
            return "Input salah";
        }
    }

    public static void Resolve (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan jam berupa format 'hh:mm:ss(AM/PM) : ");
        String hours = input.nextLine();
        String convert = timeConversion(hours);
        System.out.println(convert);
    }
}
