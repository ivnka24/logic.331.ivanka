package String;

import java.util.Scanner;

public class HackerRankInAString {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        boolean isHackerRank = false;
        int count = 0, helper = 0;
        char[] keyChars = {'h', 'a', 'c', 'k', 'e', 'r', 'r', 'a', 'n', 'k'};

        System.out.print("Input: ");
        String text = input.nextLine();

        char[] chars = text.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == keyChars[helper]){
                helper++;
                count++;
            }
            else if (count == 10) {
                isHackerRank = true;
                break;
            }
            else {
                isHackerRank = false;
            }
        }

        if (isHackerRank == true){
            System.out.println("YA");
        }
        else {
            System.out.println("TIDAK");
        }
    }
}
