package String;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan, n;
        char confirm;
        boolean flag = true;

        while (flag){
            System.out.println("Pilih Soal (1 - 10)");
            System.out.print("Pilihan: ");
            pilihan = input.nextInt();

            while (pilihan < 1 || pilihan > 12){
                System.out.println("Angka tidak tersedia !!!!!");
                pilihan = input.nextInt();
            }

            switch (pilihan){
                case 1:
                    CamelCase.Resolve();
                    break;
                case 2:
                    StrongPassword.Resolve();
                    break;
                case 3:
                    CaesarChipher.Resolve();
                    break;
                case 4:
                    MarsExploration.Resolve();
                    break;
                case 5:
                    HackerRankInAString.Resolve();
                    break;
                case 6:
                    Pangrams.Resolve();
                    break;
                case 7:
                    SeparateTheNumbers.Resolve(); // Tak paham soal
                    break;
                case 8:
                    GemStones.Resolve(); // Masih Bingung Logic
                    break;
                case 9:
                    MakingAnagrams.Resolve();
                    break;
                case 10:
                    TwoString.Resolve();
                    break;
                default:
            }

            System.out.print("Apakah anda ingin mencoba soal lain ? (y/n) ");
            confirm = input.next().charAt(0);

            if (confirm != 'y'){
                flag = false;
            }
        }
    }
}