package InputPlural;

import java.util.Scanner;

public class Task4 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int maxCount = 0;
        int mode = intArray[0];
        for (int i = 0; i < length; ++i) {
            int currentCount = 0;
            for (int j = 0; j < length; j++) {
                if (intArray[j] == intArray[i]) {
                    currentCount++;
                }
            }
            if (currentCount > maxCount) {
                maxCount = currentCount;
                mode = intArray[i];
            }
        }
        System.out.print("Hasil dari modus adalah " + mode + " dengan muncul sebanyak " + maxCount+ " ");
    }
}
