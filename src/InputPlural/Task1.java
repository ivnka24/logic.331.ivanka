package InputPlural;

import java.util.Scanner;

public class Task1 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int [] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int sum = 0;
        for (int i = 0; i < length; i++) {
            sum += intArray[i];
        }
        System.out.println("Total Hasil penjumlahan deret adalah = "+sum);
    }
}
