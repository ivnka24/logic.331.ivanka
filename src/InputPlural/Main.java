package InputPlural;

import Array2D.*;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-12)");
        int pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12) {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        switch (pilihan) {
            case 1:
                Task1.Resolve();
                break;
            case 2:
                Task2.Resolve();
                break;
            case 3:
                Task3.Resolve();
                break;
            case 4:
                Task4.Resolve();
                break;
            case 5:

                break;
            case 6:

                break;
            case 7:

                break;
            case 8:

                break;
            case 9:

                break;
            case 10:

                break;
            case 11:

                break;
            case 12:

                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
    }
}