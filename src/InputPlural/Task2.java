package InputPlural;

import java.util.Scanner;

public class Task2 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int [] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int sum = 0;
        double avarage;
        for (int i = 0; i < length; i++) {
            sum += intArray[i];
        }
        avarage = sum/length;
        System.out.println("Total Hasil avarage deret adalah = "+avarage);
    }
}
