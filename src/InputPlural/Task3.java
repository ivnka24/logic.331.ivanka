package InputPlural;

import java.util.Scanner;

public class Task3 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input deret angka");
        String text = input.nextLine();

        int[] intArray = Utility.ConvertStringToArrayInt(text);
        int length = intArray.length;
        int pointmed = 0;
        double med = 0;
        int sk1 = 0, sk2 = 0;
        for (int i = 0; i < length; i++) {
            if (length % 2 == 1) {
                pointmed = length / 2;
                med = intArray[pointmed];
            } else {
                pointmed = length / 2;
                sk1 = intArray[pointmed-1];
                sk2 = intArray[pointmed];
                med = (sk1 + sk2)/2;
            }
        }
        System.out.println("Hasil median deret adalah = " + med);
    }
}
