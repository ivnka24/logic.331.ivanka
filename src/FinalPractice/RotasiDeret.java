package FinalPractice;

import java.util.Scanner;

public class RotasiDeret {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int[] numbers = {7, 3, 9, 9, 2};
        int rotasi, helper1 = 0, helper2 = 0;

        System.out.println("Deret angka");
        for (int number : numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
        System.out.print("Anda ingin melakukan berapa kali rotasi: ");
        rotasi = input.nextInt();

        for (int i = 0; i < rotasi; i++) {
            for (int j = 1; j < numbers.length; j++) {
                helper1 = numbers[0];
                System.out.println(helper1);
                if (j == numbers.length - 1){
                    numbers[j] = helper1;
                }
                else {
                    helper2 = numbers[j];
                    numbers[j - 1] = helper2;
                }
            }
        }

        System.out.print("Hasil Rotasi: ");
        for (int number: numbers) {
            System.out.print(number + " ");
        }
        System.out.println();
    }
}
