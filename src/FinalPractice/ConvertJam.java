package FinalPractice;

import java.util.Scanner;

public class ConvertJam {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String waktu, format, subWaktu;

        System.out.print("Masukan waktu: ");
        waktu = input.nextLine();

        subWaktu = waktu.substring(0, 8);
        format = waktu.substring(9, 11);

        String[] arrSubWaktu = subWaktu.split(":");
        int[] intSubWaktu  = new int[arrSubWaktu.length];

        for (int i = 0; i < intSubWaktu.length; i++) {
            intSubWaktu[i] = Integer.parseInt(arrSubWaktu[i]);
        }

        if (format.equalsIgnoreCase("PM")){
            intSubWaktu[0] += 12;
            System.out.println(intSubWaktu[0] + ":" + intSubWaktu[1] + ":" + intSubWaktu[2]);
        }
        else {
            System.out.println(intSubWaktu[0] + ":" + intSubWaktu[1] + ":" + intSubWaktu[2]);
        }
    }
}
