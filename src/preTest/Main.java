package preTest;

import InputPlural.Task1;
import Warmup.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-12)");
        int pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12) {
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        switch (pilihan) {
            case 1:
                task1.Resolve();
                break;
            case 2:
                task2.Resolve();
                break;
            case 3:
                task3.Resolve();
                break;
            case 4:
                task4.Resolve();
                break;
            case 5:
                task5.Resolve();
                break;
            case 6:
                task6.Resolve();
                break;
            case 7:
                task7.Resolve();
                break;
            case 8:
                task8.Resolve();
                break;
            case 9:
                task9.Resolve();
                break;
            case 10:
                task10.Resolve();
                break;
            case 11:

                break;
            case 12:

                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
    }
}
