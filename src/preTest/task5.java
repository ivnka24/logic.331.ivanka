package preTest;

import java.util.HashMap;
import java.util.Scanner;

public class task5 {
//    1 orang dewasa laki-laki = 2 piring nasi goreng
//    1 orang dewasa perempuan = 1 piring mie goreng
//    2 orang remaja = 2 mangkok mie ayam = 1
//    1 orang anak-anak memakan = 1/2 piring nasi goreng
//    1 orang balita memakan = 1 mangkok kecil bubur
//    explain input 3 orang dewasa = 6 porsi, perempuan dewasa = 1 orang, balita = 1 orang, 2
    public static void Resolve (){
        Scanner input = new Scanner(System.in);
        System.out.println("Masukan Data : ");
        String text = input.nextLine();
//        input = orang dewasa : 1, perempuan dewasa : 2
//                  key        : value
        String [] textArray = text.split(",");
        HashMap<String, Integer> listText = new HashMap<>();
        for (int i = 0; i < textArray.length; i++) {
            String [] splitTitikdua = textArray[i].split(":");
            String key = splitTitikdua[0];
            int value = Integer.parseInt(splitTitikdua[1]);
            if(listText.containsKey(key)){
                int nilaiLama = listText.get(key);
                int nilaiBaru = nilaiLama + value;
                listText.replace(key, nilaiLama, nilaiBaru);
            } else  {
                listText.put(key, value);
            }
        }
        String [] validation = new String[listText.size()];
        int index = 0;
        int sumPorsi = 0;
        double currentSum = 0.0;
        for (String list: listText.keySet()) {
            validation[index] = list;

            if(validation[index].equalsIgnoreCase("Laki Laki Dewasa")){
                currentSum = listText.get(list) * 2;
            } else if (validation[index].equalsIgnoreCase("Perempuan Dewasa")) {
                currentSum = listText.get(list) * 1;
            } else if (validation[index].equalsIgnoreCase("Remaja")) {
                currentSum = listText.get(list) * 1;
            } else if (validation[index].equalsIgnoreCase("Anak-Anak")) {
                currentSum = (listText.get(list) * 0.5);
            } else if (validation[index].equalsIgnoreCase("Balita")) {
                currentSum = listText.get(list) * 1;
            }
            index++;
            sumPorsi +=currentSum;
        }
        System.out.println("Porsi : "+sumPorsi);
    }
}
