package preTest;

import java.util.ArrayList;
import java.util.Scanner;

public class task2 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        String kalimat;
        ArrayList<Character> hurufVokal = new ArrayList<>();
        ArrayList<Character> hurufKonsonan = new ArrayList<>();

        System.out.print("Input kalimat: ");
        kalimat = input.nextLine();

        String[] kalimats = kalimat.toLowerCase().split(" ");

        for (int i = 0; i < kalimats.length; i++) {
            char[] hurufs = kalimats[i].toCharArray();
            for (int j = 0; j < hurufs.length; j++) {
                if (hurufs[j] == 'a' || hurufs[j] == 'i' || hurufs[j] == 'u' || hurufs[j] == 'e' || hurufs[j] == 'o'){
                    hurufVokal.add(hurufs[j]);
                }
                else {
                    hurufKonsonan.add(hurufs[j]);
                }
            }
        }

        System.out.print("Huruf vokal: ");
        for (char v : hurufVokal) {
            System.out.print(v);
        }

        System.out.println();

        System.out.print("Huruf konsonan: ");
        for (char k : hurufKonsonan) {
            System.out.print(k);
        }

        System.out.println();
    }
}
