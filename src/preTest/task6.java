package preTest;

import java.util.Scanner;

public class task6 {

    private static Scanner input = new Scanner(System.in);
    private static String pin;
    private static int deposit = 0;
    private static int currentSaldo = 0;

    private static String codeBank;

    private static String rek;

    public static void Resolve() {
        Createpin();
    }

    public static void Createpin() {
        System.out.println("Masukan PIN yang ingin anda buat");
        pin = input.nextLine();
        if (pin.length() < 6 && pin.length() > 6) {
            System.out.println("PIN yang anda masukan kurang");
        } else if (pin.length() == 6) {
            ValidationPin();
            System.out.println("Pin berhasil di buat");
        }
    }

    public static void ValidationPin() {
        System.out.println("Masukan PIN : ");
        String textPin = input.nextLine();
        if (textPin.equalsIgnoreCase(pin)) {
            System.out.println("Berhasil masuk");
            insertDeposit();
        } else {
            System.out.println("Pin yang anda masukan salah");
        }
    }

    public static void insertDeposit() {
        System.out.println("Masukan Deposit yang anda inginkan :");
        deposit = input.nextInt();
        menuChoose();
    }

    public static void menuChoose() {
        System.out.println("1.Transfer Antar Bank    2.Transfer Antar Rekening");
        int pilihan = input.nextInt();
        switch (pilihan) {
            case 1:
                rekBedaBankValidation();
                break;
            case 2:
                rekSamaValidation();
                break;
            default:
        }
    }

    public static void rekSamaValidation() {
        input.nextLine();
        System.out.println("Masukan No Rekening Tujuan :");
        String rek = input.nextLine();
        if (rek.length() < 10 && rek.length() > 10) {
            System.out.println("PIN yang anda masukan salah");
        } else {
            transferRekening();
        }
    }

    public static void rekBedaBankValidation() {
        input.nextLine();
        System.out.println("Masukan Kode Bank :");
        codeBank = input.nextLine();
        if (codeBank.length() < 3 && codeBank.length() > 3) {
            System.out.println("Code bank yang anda masukan salah");
        } else {
            System.out.println("Masukan No Rekening Tujuan :");
            String rek = input.nextLine();
            if (rek.length() < 10 && rek.length() > 10) {
                System.out.println("PIN yang anda masukan salah");
            } else {
                transferAntarBank();
            }
        }
    }

    public static void transferRekening() {
        System.out.println("Masukan jumlah transfer : ");
        int transfer = input.nextInt();
        if (transfer >= deposit) {
            System.out.println("Transfer melebihi saldo, saldo anda tidak cukup");
        } else {
            currentSaldo = deposit - transfer;
            System.out.println("Transaksi berhasil, saldo anda saat ini :");
            System.out.println("Saldo anda sisa : " + currentSaldo);
        }
    }

    public static void transferAntarBank() {
        System.out.println("Masukan jumlah transfer : ");
        int transfer = input.nextInt();
        int saldo = transfer + 7500;
        if (transfer >= saldo) {
            System.out.println("Transfer melebihi saldo, saldo anda tidak cukup");
        } else {
            currentSaldo = deposit - saldo;
            System.out.println("Transaksi berhasil, saldo anda saat ini :");
            System.out.println("Saldo anda sisa : " + currentSaldo);
        }
    }
}
