package preTest;

import java.util.Scanner;

//Toko ke Tempat 1 = 2km
//Tempat 1 ke Tempat 2 = 500
//Tempat 2 ke Tempat 3 = 1.5
//Tempat 3 ke Tempat 4 = 2.5
// Toko ke Tempat 1 - Tempat 2 - Toko = 2 liter
// 5 km ?  2km -      500      - 2,5 km = 5km
// 1 Liter Bensin dapat digunakan 2,5 km
// 0 + 2 + 0.5 + 1.5
//
public class task4 {
    public static void Resolve() {
//                            toko,t1,  t2,  t3,  t4
        double[] distances = {0.0, 2.0, 2.5, 4.0, 6.5};
        System.out.println("Masukan Track Perjalanan : ");
        Scanner input = new Scanner(System.in);
        String text = input.nextLine();
        String[] nameLocation = {"toko", "tempat1", "tempat2", "tempat3", "tempat4"};
        String[] locationInput = text.split(" - ");
        int[] savePosition = new int[locationInput.length];
        double bensin = 0.0;
        double bensinFix = 2.5;
        double sumDistances = 0.0;
        for (int i = 0; i < locationInput.length; i++) {
            for (int j = 0; j < nameLocation.length; j++) {
                if (locationInput[i].equalsIgnoreCase(nameLocation[j])) {
                    savePosition[i] = j;
                }
            }
        }
        double tokoSebelum = 0;
        for (int index : savePosition) {
            int toko = index;
            double tokoSekarang = Math.abs(distances[toko] - tokoSebelum);
            sumDistances += tokoSekarang;
            tokoSebelum = distances[toko];
        }

        bensin = sumDistances / bensinFix;
        System.out.println(bensin);
    }

}
