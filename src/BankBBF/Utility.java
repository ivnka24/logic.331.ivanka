package BankBBF;

public class Utility {
    public static boolean isNumeric(String numberString) {
        if (numberString == null) {
            return false;
        }
        try {
            int validation = Integer.parseInt(numberString);
            return true;
        } catch (NumberFormatException exception) {
            return false;
        }
    }
}
