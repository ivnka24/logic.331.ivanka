package BankBBF;

import java.util.Scanner;

public class Transfer {
    private static Scanner input = new Scanner(System.in);
    public static int TransferAmount;

    public static void transferWithAccount() {
        System.out.print("Masukkan nomor rekening penerima (7 angka): ");
        String recipientAccount = input.nextLine();
        if (isNumberRekeningCorrect(recipientAccount)) {
            System.out.print("Masukkan jumlah uang yang akan ditransfer: ");
            TransferAmount = input.nextInt();

            if (TransferAmount > Deposit.saldo) {
                System.out.println("Saldo tidak mencukupi. Transfer gagal.");
                askForAnotherTransaction();
            } else {
                Deposit.saldo -= TransferAmount;
                System.out.println("Transfer berhasil. Saldo Anda sekarang: " + Deposit.saldo);
                askForAnotherTransaction();
            }
        } else {
            System.out.println("Nomor rekening penerima harus (7 angka)");
            transferWithAccount();
        }
    }

    public static void transferToOtherBank() {
        System.out.print("Masukkan kode bank awal (3 angka): ");
        String bankCode = input.nextLine();
        if (ValidationCodeBank(bankCode) && Utility.isNumeric(bankCode)) {
            System.out.print("Masukkan nomor rekening penerima (7 angka): ");
            String recipientAccount = input.nextLine();
            if (isNumberRekeningCorrect(recipientAccount) && Utility.isNumeric(bankCode)) {
                System.out.print("Masukkan jumlah uang yang akan ditransfer: ");
                TransferAmount = input.nextInt();

                if (TransferAmount > Deposit.saldo) {
                    System.out.println("Saldo tidak mencukupi. Transfer gagal.");
                    askForAnotherTransaction();
                } else {
                    Deposit.saldo -= TransferAmount;
                    System.out.println("Transfer berhasil. Saldo Anda sekarang: " + Deposit.saldo);
                    askForAnotherTransaction();
                }
            }else{
                System.out.println("Nomor rekening penerima harus (7 angka)");
                transferToOtherBank();
            }
        }else {
            System.out.println("Nomor kode bank awal wajib (3 angka) dan harus angka");
            transferToOtherBank();
        }
    }

    public static void askForAnotherTransaction() {

        System.out.print("Apakah Anda ingin melakukan transaksi lain? (y/n): ");
        String continueTransaction = input.next();

        if (continueTransaction.equalsIgnoreCase("y")) {
            InsertPin.validationInsert();
        } else {
            System.out.println("Terima kasih. Program selesai.");
            System.exit(0);
        }

    }

    public static boolean isNumberRekeningCorrect(String number) {
        return (number.length() == 7);
    }

    public static boolean ValidationCodeBank(String number) {
        return (number.length() == 3);
    }
}
