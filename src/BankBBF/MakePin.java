package BankBBF;

import java.util.Scanner;

public class MakePin {
    private static Scanner input = new Scanner(System.in);
    public static String pin;

    public static void makePin() {
            System.out.println("Buat PIN ATM (6 huruf) : ");
            pin = input.nextLine();
            if (Utility.isNumeric(pin) && isValidPin(pin)) {
                System.out.println("PIN Berhasil di buat");
            } else {
                System.out.println("PIN Tidak Valid, Harus 6 Angka dan Numerik");
                makePin();
            }
    }

    public static boolean isValidPin(String pinNumber) {
        return (pinNumber.length() == 6);
    }
}
