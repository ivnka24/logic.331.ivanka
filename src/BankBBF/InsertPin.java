package BankBBF;

import java.util.Scanner;

public class InsertPin {
    private static Scanner input = new Scanner(System.in);
    private static String pin;
    private static int tries = 0;
    private static int maxTries = 3;


    public static void validationInsert() {
        while (tries < maxTries) {
            System.out.println("Masukan PIN ATM Anda : ");
            pin = input.nextLine();
            if (MakePin.pin.equals(pin)) {
                System.out.println("Berhasil Masuk");
                Menu.MainMenu();
                break;
            } else {
                tries++;
                if(tries < maxTries){
                    System.out.println("PIN salah. sisa percobaan : " + (maxTries - tries));
                }else{
                    System.out.println("PIN salah sebanyak 3x. Akses ATM anda di blokir");
                    System.exit(0);
                }
            }
        }
    }
}
