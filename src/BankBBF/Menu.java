package BankBBF;

import java.util.Scanner;

public class Menu {
    private static Scanner input = new Scanner(System.in);
    private static int choice;

    public static void MainMenu() {
        boolean flag = true;
        while (flag) {
            System.out.println("MENU:");
            System.out.println("1. Setoran Tunai");
            System.out.println("2. Transfer");
            System.out.println("3. Exit");
            System.out.print("Pilih menu (1/2/3): ");

            choice = input.nextInt();

            switch (choice) {
                case 1:
                    Deposit.depo();
                    flag = false;
                    break;
                case 2:
                    TransferMenu();
                    flag = false;
                    break;
                case 3:
                    System.out.println("Terima kasih. Program selesai.");
                    flag = false;
                    return;
                default:
                    System.out.println("Menu tidak valid. Silakan pilih menu yang benar.");
            }
        }
    }

    public static void TransferMenu() {
        System.out.println("Pilihan transfer:");
        System.out.println("1. Antar Rekening");
        System.out.println("2. Antar Bank");
        System.out.print("Pilih jenis transfer (1/2): ");
        int transferType = input.nextInt();
        switch (transferType) {
            case 1:
                Transfer.transferWithAccount();
                break;
            case 2:
                Transfer.transferToOtherBank();
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
}
