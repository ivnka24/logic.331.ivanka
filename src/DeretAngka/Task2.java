package DeretAngka;

public class Task2 {
    public static void Resolve(int n){
        int genap = 2;
        int [] result = new int[n];

        for (int i = 0; i < n; i++) {
            result[i] = genap;
            genap += 2;
        }
        Utility.PrintArray(result);
    }
}
