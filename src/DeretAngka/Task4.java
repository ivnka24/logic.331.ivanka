package DeretAngka;

public class Task4 {
    public static void Resolve(int n){
        int genap = 1;
        int [] result = new int[n];

        for (int i = 0; i < n; i++) {
            result[i] = genap;
            genap += 4;
        }
        Utility.PrintArray(result);
    }
}
