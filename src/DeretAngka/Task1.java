package DeretAngka;

public class Task1 {

    public static void Resolve(int n){
        int ganjil = 1;
        int [] result = new int[n];

        for (int i = 0; i < n; i++) {
            result[i] = ganjil;
            ganjil += 2;
        }
        Utility.PrintArray(result);
    }
}
