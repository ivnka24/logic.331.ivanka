package DeretAngka;

public class Task5 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int value = 1;
        int count = 4;
        for (int i = 0; i < n; i++) {
            if (count % 3 == 0) {
                result[i] = 0;
                count += 1;
            } else {
                result[i] += value;
                value += 4;
                count += 1;
            }
        }
        Utility.PrintArray(result);
    }
}
