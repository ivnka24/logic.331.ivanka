package DeretAngka;

public class Task8 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int value = 3;
        for (int i = 0; i < n; i++) {
            result[i] = value;
            value *= 3;
        }
        Utility.PrintArray(result);
    }
}
