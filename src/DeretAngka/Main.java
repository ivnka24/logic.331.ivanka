package DeretAngka;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Pilihan Soal (1-12)");
        int pilihan = input.nextInt();

        while(pilihan < 1 || pilihan > 12){
            System.out.println("Angka tidak tersedia");
            pilihan = input.nextInt();
        }
        System.out.println("Masukan Nilai n = ");
        int n = input.nextInt();

        switch (pilihan){
            case 1 :
                Task1.Resolve(n);
                break;
            case 2 :
                Task2.Resolve(n);
                break;
            case 3 :
                Task3.Resolve(n);
                break;
            case 4:
                Task4.Resolve(n);
                break;
            case 5:
                Task5.Resolve(n);
                break;
            case 6:
                Task6.Resolve(n);
                break;
            case 7:
                Task7.Resolve(n);
                break;
            case 8:
                Task8.Resolve(n);
                break;
            case 9:
                Task9.Resolve(n);
                break;
            case 10:
                Task10.Resolve(n);
                break;
            case 11:
                Task11.Resolve(n);
                break;
            case 12:
                Task12.Resolve(n);
                break;
            default:
                System.out.println("Angka yang dimasukan tidak valid");
        }
        }
    }