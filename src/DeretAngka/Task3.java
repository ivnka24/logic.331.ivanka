package DeretAngka;

public class Task3 {
    public static void Resolve(int n){
        int genap = 1;
        int [] result = new int[n];

        for (int i = 0; i < n; i++) {
            result[i] = genap;
            genap += 3;
        }
        Utility.PrintArray(result);
    }
}
