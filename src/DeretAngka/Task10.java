package DeretAngka;

public class Task10 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int stack = 0;
        int value = 3;
        for (int i = 0; i < n; i++) {
            stack++;
            result[i] = value;
            value *= 3;
            if (stack % 4 == 0) {
                result[i] = 0;
            }
        }
        Utility.PrintArray(result);
    }
}
