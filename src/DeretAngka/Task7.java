package DeretAngka;

public class Task7 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int value = 2;
        for (int i = 0; i < n; i++) {
            result[i] = value;
            value *= 2;
        }
        Utility.PrintArray(result);
    }
}
