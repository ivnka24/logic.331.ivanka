package DeretAngka;

public class Task12 {
    public static void Resolve(int n) {
        int count = 2;
        int trigger = 2;
        int dump = 0;
        int[] result = new int[n];
        for (int i = 0; i < n; i++) {
            if (count % trigger == 0){
                count ++;
                trigger ++;
                dump = count;
                result [i] = dump;
            }else {
                count ++;
                trigger ++;
                dump = trigger;
                result[i] = dump;
            }
        }
        Utility.PrintArray(result);

    }
}
