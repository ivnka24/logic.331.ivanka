package DeretAngka;

public class Task11 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int awal = 1;
        int sebelum = 0;
        for (int i = 0; i < n; i++) {
            result[i] = awal;
            if (i > 0) {
                sebelum = result[i-1];
                awal += sebelum;
            }else {
                awal += sebelum;
            }
        }
        Utility.PrintArray(result);
    }
}
