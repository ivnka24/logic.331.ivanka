package DeretAngka;

public class Task6 {
    public static void Resolve(int n) {
        int[] result = new int[n];
        int value = 1;
        for (int i = 0; i < n; i++) {
            if (value % 3 == 0) {
                result[i] = 0;
                value += 4;
            } else {
                result[i] = value;
                value += 4;
            }
        }
        Utility.PrintArray(result);
    }
}
