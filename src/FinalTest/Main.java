package FinalTest;

import InputPlural.*;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        menu();
    }

    public static void menu(){
        Scanner input = new Scanner(System.in);
        System.out.println("===== MENU =====");
        System.out.println("Masukkan nomor soal dari 1 - 10");
        int pilihan = input.nextInt();
        while (pilihan < 0 || pilihan > 10){
            System.out.println("Pilihan tidak sesuai, Pastikan sesuai dengan Menu!");
            System.out.println("Pilihan : ");
            pilihan = input.nextInt();
        }

        switch (pilihan){
            case 1 :
                Loli01.resolve();
                break;
            case 2 :
                Palindrom02.resolve();
                break;
            case 3 :
                rotasi03.resolve();
                break;
            case 4 :

                break;
            case 5 :
                formatWaktu05.resolve();
                break;
            case 6 :
                Perpustakaan06.resolve();
                break;
            case 7 :
                Deret07.resolve();
                break;
            case 8 :
                // Tidak ada nomor 8
                break;
            case 9 :
                Derajat09.resolve();
                break;
            case 10 :
                Cupcake10.resolve();
                break;
            default:

        }
    }
}
