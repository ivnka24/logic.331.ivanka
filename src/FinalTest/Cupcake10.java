package FinalTest;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Cupcake10 {
    public static void resolve(){ // 4 menit (SOAL AMBIGU) Nyari recipe per cupcake dengan nilai sesuai soal atau ? nyari perbandingan cupcake dengan n dan bahan sisanya
        Scanner input = new Scanner(System.in);
        double terigu = (double) 125 / 15;
        double gula = (double) 100 / 15;
        double susu = (double) 100 / 15;
        System.out.println("Masukkan banyak cupcake = ");
        int inputCupcake = input.nextInt();

        double outputTerigu = terigu * inputCupcake;
        double outputGula = gula * inputCupcake;
        double outputSusu = susu * inputCupcake;

        DecimalFormat df = new DecimalFormat("0,0000");

        System.out.println("Output Perbandingan satu Cupcake: ");
        System.out.println("Trigu = " + df.format(outputTerigu));
        System.out.println("Gula Pasir = " + df.format(outputGula));
        System.out.println("Susu = " + df.format(outputSusu));
    }
}
