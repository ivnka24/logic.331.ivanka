package FinalTest;

import java.util.Arrays;
import java.util.Scanner;

public class Deret07 {
    public static void resolve(){
        Scanner input = new Scanner(System.in);
        System.out.println("=====> DERET ANGKA <=====");
        System.out.println("Contoh input  : 8 7 0 2 7 1 7 6 3 0 7 1 3 4 6 1 6 4 3");
        System.out.println("Masukkan deret angka : ");
        String inputString = input.nextLine();
        String[] splitInput = inputString.split(" ");
        int[] angkaInput = new int[splitInput.length];
        String[] output = new String[3];

        for (int i = 0; i < angkaInput.length; i++) {
            angkaInput[i] = Integer.parseInt(splitInput[i]);
        }
        // Olah Mean rata²
        int mean = 0;
        for (int i = 0; i < angkaInput.length; i++) {
            mean += angkaInput[i];
        }
        mean /= angkaInput.length;
        output[0] = String.valueOf(mean);
        // Olah median (nilai tengah) harus urut dulu
        double tengah = 0;
        int[] urut = angkaInput.clone();
        Arrays.sort(urut);
        //
        System.out.println("cek urutan");
        for (int i = 0; i < urut.length; i++) {
            System.out.print(urut[i] + " ");
        }
        System.out.println();
        //
        if (urut.length % 2 == 0){
            tengah = urut[(urut.length /2)] + urut[(urut.length /2) - 1];
            output[1] = String.valueOf(tengah/2);
        }else {
            tengah = urut[(urut.length /2)];
            output[1] = String.valueOf(tengah);
        }
        // Olah Modus
        int nilaiModus = 0;
        int banyakModus = 0;
        int hitung = 0;
        boolean cekNilaiLama = false;
        for (int i = 0; i < angkaInput.length; i++) {
            int nilaiSekarang = angkaInput[i];
            for (int j = 0; j < i; j++) {
                int ambilNilai = angkaInput[j];
                if (ambilNilai == nilaiSekarang){
                    cekNilaiLama = true;
                }else {
                    cekNilaiLama = false;
                }
            }
            if (cekNilaiLama){
                break;
            }
            for (int j = 0; j < angkaInput.length; j++) {
                int ambilNilai = angkaInput[j];
                if (nilaiSekarang == ambilNilai){
                    hitung++;
                }
            }
            if (hitung == banyakModus && nilaiSekarang < nilaiModus){
                nilaiModus = nilaiSekarang;
                banyakModus = hitung;
            } else if (hitung > banyakModus) {
                nilaiModus = nilaiSekarang;
                banyakModus = hitung;
            }

            if (i == 0){
                nilaiModus = nilaiSekarang;
                banyakModus = hitung;
            }
            hitung = 0;
        }
        output[2] = String.valueOf(nilaiModus);

        System.out.println("Output");
        System.out.println("Nilai Mean = " + output[0]);
        System.out.println("Nilai Median = " + output[1]);
        System.out.println("Nilai Modus = " + output[2]);
    }
}
